const express = require('express');

const User = require('../models/User');
const Track = require('../models/Track');
const TrackHistory = require('../models/TrackHistory');

const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    const token = req.get('Token');

    if (!token) res.status(401).send({error: 'Unauthorized'});

    User.find({token: token})
      .then(response => {
        TrackHistory.find({user: response._id})
          .then(response => res.send(response))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  });

  router.post('/', (req, res) => {
    const token = req.get('Token');

    if (!token) res.status(401).send({error: 'Unauthorized'});

    // const user = await User.findOne({token: token});
    //
    // const track = await Track.findById(req.body.track);
    //
    // const tracksHistoryData = {
    //   user: user._id,
    //   track: track._id,
    //   datetime: new Date()
    // };
    //
    // const tracksHistory = new TrackHistory(tracksHistoryData);
    //
    // tracksHistory.save()
    //   .then(result => res.send(result))
    //   .catch(error => res.status(400).send(error))

    User.findOne({token: token})
      .then(response => {
        const userId = response._id;

        Track.findById(req.body.track)
          .then(response => {
            const tracksHistoryData = {
              user: userId,
              track: response._id,
              datetime: new Date()
            };

            const tracksHistory = new TrackHistory(tracksHistoryData);

            tracksHistory.save()
              .then(result => res.send(result))
              .catch(error => res.status(400).send(error));
          })
          .catch(error => res.status(400).send(error))
      })
      .catch(() => res.status(404).send({error: 'User not found'}));
  });

  return router;
};

module.exports = createRouter;